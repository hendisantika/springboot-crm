package com.hendisantika.springbootcrm.formatter;

import com.hendisantika.springbootcrm.model.City;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/04/20
 * Time: 11.01
 */
public class CityFormatter implements Formatter<City> {

    @Override
    public String print(City hobby, Locale locale) {
        return hobby.getId().toString();
    }

    @Override
    public City parse(String id, Locale locale) throws ParseException {
        City hobby = new City();
        hobby.setId(Integer.parseInt(id));
        return hobby;
    }
}