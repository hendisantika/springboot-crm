package com.hendisantika.springbootcrm.formatter;

import com.hendisantika.springbootcrm.model.Role;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/20
 * Time: 18.59
 */
public class RoleFormatter implements Formatter<Role> {

    @Override
    public String print(Role role, Locale locale) {
        return role.getId().toString();
    }

    @Override
    public Role parse(String id, Locale locale) throws ParseException {
        Role role = new Role();
        role.setId(Integer.parseInt(id));
        return role;
    }
}