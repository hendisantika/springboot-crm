package com.hendisantika.springbootcrm.formatter;

import com.hendisantika.springbootcrm.model.Permission;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/20
 * Time: 19.00
 */
public class PermissionFormatter implements Formatter<Permission> {

    @Override
    public String print(Permission hobby, Locale locale) {
        return hobby.getId().toString();
    }

    @Override
    public Permission parse(String id, Locale locale) throws ParseException {
        Permission hobby = new Permission();
        hobby.setId(Integer.parseInt(id));
        return hobby;
    }
}