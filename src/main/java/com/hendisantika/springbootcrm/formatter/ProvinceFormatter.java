package com.hendisantika.springbootcrm.formatter;

import com.hendisantika.springbootcrm.model.Province;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/04/20
 * Time: 11.02
 */
public class ProvinceFormatter implements Formatter<Province> {

    @Override
    public String print(Province hobby, Locale locale) {
        return hobby.getId().toString();
    }

    @Override
    public Province parse(String id, Locale locale) throws ParseException {
        Province hobby = new Province();
        hobby.setId(Integer.parseInt(id));
        return hobby;
    }
}