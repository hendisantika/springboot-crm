package com.hendisantika.springbootcrm.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/04/20
 * Time: 08.09
 */
@SuppressWarnings("serial")
public class BadRequestException extends RuntimeException {

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable ex) {
        super(message, ex);
    }

    public BadRequestException() {
        super("");
    }

}
