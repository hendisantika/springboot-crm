package com.hendisantika.springbootcrm.repository;

import com.hendisantika.springbootcrm.model.Permission;
import com.hendisantika.springbootcrm.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/04/20
 * Time: 19.27
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
    @Query("SELECT r FROM Role r LEFT JOIN FETCH r.permissions p where r.id=:id")
    Role findRoleAndPermissionByRoleId(@Param("id") Integer id);

    @Query("SELECT p FROM Permission p")
    List<Permission> findAllPermissions();

    @Query("SELECT p FROM Permission p where id=1")
    Permission getTestPermission();

    Role findByName(String name);
}
