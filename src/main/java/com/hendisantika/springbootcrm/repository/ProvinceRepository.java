package com.hendisantika.springbootcrm.repository;

import com.hendisantika.springbootcrm.model.Province;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/04/20
 * Time: 07.23
 */
public interface ProvinceRepository extends JpaRepository<Province, Long> {
}
