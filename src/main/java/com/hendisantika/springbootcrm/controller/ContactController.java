package com.hendisantika.springbootcrm.controller;

import com.hendisantika.springbootcrm.model.Contact;
import com.hendisantika.springbootcrm.model.Province;
import com.hendisantika.springbootcrm.repository.CityRepository;
import com.hendisantika.springbootcrm.repository.ProvinceRepository;
import com.hendisantika.springbootcrm.service.ContactService;
import com.hendisantika.springbootcrm.validator.ContactValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/04/20
 * Time: 11.02
 */
@Controller
@RequestMapping(value = "/contact")
public class ContactController {

    private final String MODULE_TEMPLATE_ROOT = "admin/module/contact/";

    @Autowired
    private ContactService contactService;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private CityRepository cityRepository;

    @GetMapping(value = "/create")
    public String create(Model model) {

        List<Province> provinces = provinceRepository.findAll();
        model.addAttribute("provinces", provinces);

        model.addAttribute("entityForm", new Contact());

        return MODULE_TEMPLATE_ROOT + "create";
    }

    @PostMapping(value = "/store")
    public String store(@ModelAttribute("entityForm") Contact contactForm,
                        BindingResult bindingResult, final RedirectAttributes redirectAttributes) {

        ContactValidator contactValidator = new ContactValidator();
        contactValidator.validate(contactForm, bindingResult);

        if (bindingResult.hasErrors()) {

            return MODULE_TEMPLATE_ROOT + "create";
        } else {
            contactService.save(contactForm);
            redirectAttributes.addFlashAttribute("message", "Successfully added..");
            return "redirect:/contact/index";
        }
    }
}
