package com.hendisantika.springbootcrm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/04/20
 * Time: 11.04
 */
@Entity
@Table(name = "role")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "title", columnDefinition = "VARCHAR(70)")
    private String title;

    @Column(name = "side", nullable = false, columnDefinition = "TINYINT")
    private Integer side;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    @ManyToMany
    @JoinTable(
            name = "role_permission",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id")
    )
    private Set<Permission> permissions;

    public boolean hasPermission(String permissionStr) {

        for (Permission permission : permissions) {

            if (permissionStr.equals(permission.getName())) {
                return true;
            }

        }

        return false;
    }

    public boolean hasPermission(Permission permission) {

        for (Permission p : this.permissions) {
            if (p.getId() == permission.getId()) {
                return true;
            }
        }
        return false;
    }
}
