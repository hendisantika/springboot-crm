package com.hendisantika.springbootcrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SpringbootCrmApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootCrmApplication.class, args);
    }

}
