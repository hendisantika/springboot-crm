package com.hendisantika.springbootcrm.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/04/20
 * Time: 20.30
 */
@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
/*@Profile("dev")*/
@PropertySource({"classpath:config/persistence_${spring.profiles.active}.properties", "classpath:config/mvcconfig_$" +
        "{spring.profiles.active}.properties"})
public class MvcConfig implements WebMvcConfigurer {
   /*
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
          .addResourceHandler("/file/**")
          .addResourceLocations("/"+Paths.get("file").toString()+"/");
    }
	*/

    @Value("${mvcconfig.cookieName}")
    private String mvcconfigCookieName;

    @Value("${mvcconfig.cookieMaxAge}")
    private String mvcconfigCookieMaxAge;

    @Value("${mvcconfig.fileDirectoryResourceLocations}")
    private String mvcconfigFileDirectoryResourceLocations;

    @Override
    public void addFormatters(FormatterRegistry formatterRegistry) {
        formatterRegistry.addFormatter(new PermissionFormatter());
        formatterRegistry.addFormatter(new RoleFormatter());
        formatterRegistry.addFormatter(new ProvinceFormatter());
        formatterRegistry.addFormatter(new CityFormatter());
    }

    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public CookieLocaleResolver localeResolver() {
        CookieLocaleResolver localeResolver = new CookieLocaleResolver();
        localeResolver.setDefaultLocale(Locale.ENGLISH);
        localeResolver.setCookieName(mvcconfigCookieName);
        localeResolver.setCookieMaxAge(Integer.parseInt(mvcconfigCookieMaxAge));
        return localeResolver;
    }

    @Bean
    public LocaleChangeInterceptor localeInterceptor() {
        LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
        interceptor.setParamName("lang");
        return interceptor;
    }

  /*  @Bean
    public MenuGenerationInterceptor menuGenerationInterceptor(){
    	MenuGenerationInterceptor interceptor = new MenuGenerationInterceptor();
        return interceptor;
    }*/

 /*   @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeInterceptor());
        registry.addInterceptor(menuGenerationInterceptor());
    }*/

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/file/**")
                .addResourceLocations(mvcconfigFileDirectoryResourceLocations);
    }

    /*
    @Bean
    PropertyPlaceholderConfigurer propConfig() {
        PropertyPlaceholderConfigurer placeholderConfigurer = new PropertyPlaceholderConfigurer();
        placeholderConfigurer.setLocation(new ClassPathResource("application.properties"));
        return placeholderConfigurer;
    }*/

   /* @Bean(name = "blockService")
    public BlockService blockService() {
        return new BlockServiceImp();
    }*/
}
