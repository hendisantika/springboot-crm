package com.hendisantika.springbootcrm.service;

import com.hendisantika.springbootcrm.exception.UserAlreadyExistException;
import com.hendisantika.springbootcrm.model.User;
import com.hendisantika.springbootcrm.model.VerificationToken;
import com.hendisantika.springbootcrm.repository.RoleRepository;
import com.hendisantika.springbootcrm.repository.UserRepository;
import com.hendisantika.springbootcrm.repository.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/04/20
 * Time: 18.51
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private VerificationTokenRepository tokenRepository;

    /*
    @Override
    public User register(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(user);

        return user;
    }*/

    public User registerNewUserAccount(final User accountDto) {
        if (emailExist(accountDto.getEmail())) {
            throw new UserAlreadyExistException("There is an account with that email adress: " + accountDto.getEmail());
        }
        final User user = new User();

        // user.setFirstName(accountDto.getFirstName());
        //  user.setLastName(accountDto.getLastName());
        user.setPassword(bCryptPasswordEncoder.encode(accountDto.getPassword()));
        user.setEmail(accountDto.getEmail());
        //  user.setUsing2FA(accountDto.isUsing2FA());
        user.setRoles(new HashSet<>(Arrays.asList(roleRepository.findByName("normal_user"))));
        return userRepository.save(user);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User findUserAndRolesByUserId(long userId) {
        return userRepository.findUserAndRolesByUserId(userId);
    }

    public void createVerificationTokenForUser(User user, String token) {
        VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }

    public VerificationToken getVerificationToken(String VerificationToken) {
        return tokenRepository.findByToken(VerificationToken);
    }

    public void saveRegisteredUser(User user) {
        userRepository.save(user);
    }

    private boolean emailExist(String email) {
        User user = userRepository.findByEmail(email);
        return user != null;
    }

    public User findUserByEmail(final String email) {
        return userRepository.findByEmail(email);
    }
}
