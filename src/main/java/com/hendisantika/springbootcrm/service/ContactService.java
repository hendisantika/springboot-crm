package com.hendisantika.springbootcrm.service;

import com.hendisantika.springbootcrm.model.Contact;
import com.hendisantika.springbootcrm.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/04/20
 * Time: 07.23
 */
@Service
public class ContactService {
    @Autowired
    private ContactRepository contactRepository;

    public void save(Contact contactForm) {
        contactRepository.save(contactForm);

    }
}
