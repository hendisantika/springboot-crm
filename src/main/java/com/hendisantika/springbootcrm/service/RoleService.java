package com.hendisantika.springbootcrm.service;

import com.hendisantika.springbootcrm.model.Permission;
import com.hendisantika.springbootcrm.model.Role;
import com.hendisantika.springbootcrm.model.User;
import com.hendisantika.springbootcrm.repository.RoleRepository;
import com.hendisantika.springbootcrm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crm
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/04/20
 * Time: 19.36
 */
@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    public List<Role> findAllRoles() {
        return roleRepository.findAll();
    }

    public Role findRoleAndPermissionByRoleId(int id) {
        return roleRepository.findRoleAndPermissionByRoleId(id);
    }

    public List<Permission> findAllPermissions() {
        return roleRepository.findAllPermissions();
    }

    public void updatePermissionRole(Role role) {

        // get original role from database
        Role OriginalRole = roleRepository.findOne(role.getId());

        // update original role
        OriginalRole.setPermissions(role.getPermissions());
        roleRepository.save(OriginalRole);
	/*
	Set<Permission>	sp = new HashSet<Permission>();
	sp.add( roleRepository.getTestPermission());

	role.setSide(1);
	role.setTitle("rr");

	role.setPermissions(sp);
	*/

    }

    public void updateUserWithRoles(User user) {

        User originalUser = userRepository.findOne(user.getId());

        // update original role
        originalUser.setRoles(user.getRoles());
        userRepository.save(originalUser);

    }
}
